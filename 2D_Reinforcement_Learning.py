import time
import random
from random import choice
import matplotlib.pyplot as plt
# generate axes object
ax = plt.axes()
plt.grid()


W = 4
H = 4
# set limits
plt.xlim(0, W)
plt.ylim(0, H)
ax.set_xticks(list(i for i in range(W+1)))
ax.set_yticks(list(j for j in range(H+1)))
Unit = 1
Epoch = 200
# FreshTime = 0.5
EnvUpdTime = 0.2

class Point():
    def __init__(self, coordinates):
        self.x = coordinates[0]
        self.y = coordinates[1]
        # self.z = coordinates[2]
    def toList(self):
        return [self.x, self.y]
    def copy(self):
        return Point([self.x, self.y])

class Env():
    def __init__(self):
        self.actions = ["Left", "Right", "Up", "Down"]
        self.Begin = Point([0, 0])
        self.End = Point([W - 1, H - 1])

    def InitObj(self):
        self.Begin = Point([0, 0])
        self.End = Point([W - 1, H - 1])

    def Show(self):
        for p in ax.collections:
            p.remove()
        # b = self.Begin.toList()
        e = self.End.toList()

        # ax.scatter(b[0] + Unit / 2, b[1] + Unit / 2, color='r')
        ax.scatter(e[0] + Unit / 2, e[1] + Unit / 2, color='y')

    def Reset(self):
        time.sleep(2)

        self.InitObj()

        self.Show()

class Q_Table():
    def __init__(self, actions):
        self.table = list(dict())
        self.actions = actions

    def Insert(self, S, V = 0):
        D_action = {}
        for _a in self.actions:
            D_action[_a] = 0

        self.table.append(dict(Site=S.toList(), Action=D_action, Value=V))

    def Exist(self, S, IsAutoInsert = True):
        Result = any(d['Site'] == S.toList() for d in self.table)
        if not Result and IsAutoInsert:
            self.Insert(S)
        return Result

    def iloc(self, S = None, A = None, IsValue = True):
        # it have to only one data
        data = []
        if S is not None and A is not None:
            data = list(filter(lambda x: x['Site'] == S.toList(), self.table))[0]['Action'][A]
        elif S is not None and A is None:
            if IsValue:
                data = list(filter(lambda x: x['Site'] == S.toList(), self.table))[0]["Action"].values()
            else:
                data = list(filter(lambda x: x['Site'] == S.toList(), self.table))[0]["Action"]
        else:
            print("Check Q_Table Data and Parameter value Site = ", S.toList(), "Action = ", A)

        return data

    def GetOneBySite(self, S):
        d = list(filter(lambda x: x['Site'] == S.toList(), self.table))
        if len(d) > 2:
            print("error")
        return d[0]

    def upd_Q_table(self, S, A, value):
        # print(value)
        # print("origin Q table[", rowN, "][", colN, "] = ", self.table[rowN][colN])
        # print("Upd", rowN, colN, value)
        # self.table[rowN][colN] = self.table[rowN][colN] + value
        focusIndex = self.table.index(self.GetOneBySite(S))
        # print("original = ", self.table[focusIndex]["Action"][A], "value = ", value)
        self.table[focusIndex]["Action"][A] += value
        # print("Update = ", self.table[focusIndex]["Action"][A])
        # self.table[0][1] = 0
        # self.table[0][0] = 0
        # print("Update Q table[", rowN, "][", colN, "] = ", self.table[rowN][colN])
        #
        # for row in self.table:
        #     print(*row)

class Brain():
    def __init__(self, actions, learning_rate=0.5, reward_decay=0.95, e_greedy=0.9):
        self.actions = actions  # a list
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.BLTable = Q_Table(self.actions)

    def Choose_Actions(self, S):
        self.BLTable.Exist(S)
        _actionIndex = 0
        NowSiteValues = self.BLTable.iloc(S)

        if all(v == 0 for v in NowSiteValues) or random.random() > self.epsilon:
            _action = choice(self.actions)
            _actionIndex = self.actions.index(_action)
        else:
            GreedyNowSiteValues = self.BLTable.iloc(S, None, False)
            # print(GreedyNowSiteValues.values())
            _action = max(GreedyNowSiteValues, key=GreedyNowSiteValues.get)
            # print(_action)
            # MaxVlaue_actionsIndex = NowSiteValues.index(max(NowSiteValues))
            # _action = self.actions[MaxVlaue_actionsIndex]
            # _actionIndex = MaxVlaue_actionsIndex

        return _action

    def learning(self, s, a, r, s_):
        self.BLTable.Exist(s_)
        q_predict = self.BLTable.iloc(s, a)
        if s_ != 'terminal':
            q_target = r + self.gamma * max(self.BLTable.iloc(s_))  # next state is not terminal
        else:
            q_target = r  # next state is terminal

        value = self.lr * (q_target - q_predict)  # update
        self.BLTable.upd_Q_table(s, a, value)


# S(S) = list or str, A(Action) = str, T(Target) = list
def get_env_feedback(S, A, T):
    # This is how agent will interact with the environment
    # print(Target_index)
    _S = S.copy()
    R = 0
    done = False

    # print(S.toList(), T.toList())
    if S.toList() == T.toList():
        done = True
        S_ = 'terminal'
        R = 1
    elif A is 'Right':
        _S.x = W - Unit if _S.x + Unit > W - Unit else _S.x + Unit
    elif A is "Left":
        _S.x = 0 if _S.x - Unit < 0 else _S.x - Unit
    elif A is "Up":
        _S.y = H - Unit if _S.y + Unit > H - Unit else _S.y + Unit
    elif A is "Down":
        _S.y = 0 if _S.y - Unit < 0 else _S.y - Unit

    return _S, R, done

def upd_env(S, epoch, step_counter):
    if S == 'terminal':
        interaction = 'Episode %s: total_steps = %s' % (epoch+1, step_counter)
        print('\r{}'.format(interaction), end='')
        time.sleep(1)
        print('\r                                ', end='')
    else:
        ax.scatter(S[0] + Unit / 2, S[1] + Unit / 2, color='r')
        # ax.plot([i], [i + 1], 'rx')

        plt.pause(EnvUpdTime)  # is necessary for the plot to update for some reason

        # start removing points if you don't want all shown
        # ax.lines[0].remove()
        # print(len(ax.collections))
        ax.collections[1].remove()

def main():
    env = Env()
    B = Brain(env.actions)

    for _e in range(Epoch):
        env.Reset()
        S = env.Begin
        T = env.End
        step_counter = 0
        IsTerminal = False

        while not IsTerminal:
            upd_env(S.toList(), _e, step_counter)

            A = B.Choose_Actions(S)
            _S, R, IsTerminal = get_env_feedback(S, A, T)

            # print(A, "Original = ", S.toList(), "After Upd = ", _S.toList())
            B.learning(S, A, R, _S)

            S = _S
            step_counter += 1

            if IsTerminal:
                print("epoch =", _e + 1, "step_counter", step_counter)


if __name__ == "__main__":
    main()

    # for i in range(Epoch):
    #     upd_env([i, i], i, i+2)